# SOLID as information storage for the Situational-Context #

## WHY? ##
Advertising has become the most important source of income for a significant number of web-based companies. This income is usually dependent on the personal information that companies gather from their users which has led them to create very rich profiles of their users. However, these profiles do not follow any standard and are usually incomplete in the sense that users provide different subsets of information to each platform. In this context, the SOLID initiative proposes an alternative to decentralize the user information giving them complete ownership of their information. In this demo, we propose a proof of concept in which SOLID is used to store the user information in their mobile device, following the Situational-Context paradigm to provide this information as a service to third parties.

This repository contains all resources needed to deploy SOLID as an information storage for the Situational-Context.

![image](https://i.postimg.cc/g2LtzJSY/SOLID-full-architecture-6.png)

There are three main elements in the architecture: 1) the API Gateway, 2) the PeeaS application and 3) the SOLID PODS.

![image](https://i.postimg.cc/yYm20Fm5/Entities-Scheme.png)

Thus, it is necessary to operate with the three components in order to execute the demonstration. This procedure begins on the smartphone, deploying the local PODS and installing the PeeaS application. As a last step, the API Gateway is linked with the application and executed.

You can check the video demo in the next youtube link:

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/kFD2uDBUlhM/0.jpg)](https://youtu.be/kFD2uDBUlhM)

### Instalation process ###
The instalation process is based on three main steps.
1. Deploy the PODS (3) locally in smartphone
2. Installing the PeeaS application (2) and connect it with the API Gateway (1)
3. Demo example

## 1. Deploy PODS (3) locally in smartphone ##
The first step is deploying the PODS (3) where the user profile will be hosted. The main steps are the nexts:
1. Get Root access in your device
2. Installing NodeJS
3. Installing the Solid POD
4. Running the Solid POD

It is necessary to have these applications installed to perform the corresponding taks:
* Solid Explorer File Manager
* Termux

### 1.1 Get Root access in your device ###
The deployment of the PODS requires to modify /system/etc/hosts file. Thus, it is require to get Root access in the device. 
This process actively depends on the smartphone model. Nevertheless, most common tools to root phones are SuperSU or Magisk.

Once the administrator access is granted, /system/etc/hosts file is modified. This archive is the local DNS which has to be changed to redirects addresses like \*.localhost.
Initially, the file contains the next lines:	
~~~~
127.0.0.1	localhost
::1			ip6-localhost
~~~~

To allow the redirection to addresses like \*.localhost, the next line is added:
~~~~
127.0.0.1	*.localhost
~~~~

This way, the final file aspect is:
~~~~
127.0.0.1	localhost
127.0.0.1	*.localhost
::1			ip6-localhost
~~~~
When DNS archive is successfully configured, the next step is installing NodeJS in the device.

### 1.2 Installing NodeJS ###
NodeJS is installed on the smartphone using the command line which provides Termux app. Therefore, the commands to input follow the next sequence:
~~~~
$ apt update
$ apt upgrade
$ pkg install tsu
$ pkg install coreutils
$ pkg install nodejs
~~~~
As a result, NodeJS is installed on the smartphone.

### 1.3 Installing the SOLID PODS ###
The SOLID PODS is deployed using command line at Termux. Thus, the commands to input are:
~~~~
$ pkg install git
$ git clone https://github.com/solid/node-solid-server
$ cd node-solid-server
$ npm install -g solid-server
~~~~

Once this process is finished, just one step is left.

### 1.4 Running the SOLID PODS ###
The server requires some more configuration steps to deploy the PODS: (situated inside node-solid-server)
~~~~
$ mkdir key
$ openssl req -outform PEM -keyform PEM -new -x509 -sha256 -newkey rsa:2048 -nodes -keyout ./key/privkey.pem -days 365 -out ./key/fullchain.pem
$ tsu
$ npm run solid init
$ bash ./bin/solid-test start
~~~~

As a consequence of these steps, the PODS is finally deployed in the smartphone.

![image](https://i.postimg.cc/7LhJJB53/exec-01.png)

![image](https://i.postimg.cc/BZTbbWhW/exec-02.png)

![image](https://i.postimg.cc/9F0KP1d3/POD-03.png)

![image](https://i.postimg.cc/zD04R4RS/POD-04.png)

The PODS should be available on the smartphone navigator in the address https://localhost:8443/.

![image](https://i.postimg.cc/fyTFrf19/POD-05.png)

Since certificate is not officially signed by any entity, it is possible the navigator warns about it.

![image](https://i.postimg.cc/prG7bnt1/POD-06.png)

![image](https://i.postimg.cc/vBvkhN2y/POD-07.png)

Using the web interface, the personal information is provided in order to complete the PODS with the corresponding basic data.

![image](https://i.postimg.cc/KzbCn2NQ/POD-08.png)

Since de PODS is locally deployed, the container is not able to communicate with external applications and attend information requests. Thus, after fill it with the data, it is precise to use two additional components to provide the full architecture. In the next section, this process is explained.

## 2. Installing the PeeaS application (2) and connect it with the API Gateway (1) ##
Download the repository from Bitbucket. It is recommended to obtain it straighly from the web interface option, located in the three dots button.

![image](https://i.postimg.cc/vHptWJJR/Configure-01.png)

Next, the downloaded _zip_ is decompressed and the API Gateway (1) and PeeaS application (2) code is obtained.

![image](https://i.postimg.cc/vBSksQmq/Configure-02.png)

Next, there are two main points to communicate the PeaaS application (2) with the API Gateway (1). First, the PeaaS application (2) is installed and executed and, secondly, the API Gateway (1) is modified with this value.

### 2.1 Installing and executing the smartphone application and obtaining the Firebase TOKEN

It is recommend to use Android Studio to open the smartphone application.

To perform this, the steps are the next:

1 . Open Android Studio application

![image](https://i.postimg.cc/L4GFWhp7/Android-01.png)

2 . Open the _PeeaSapp_ project

![image](https://i.postimg.cc/NjcqgVLk/Android-02.png)

![image](https://i.postimg.cc/jqDrvPGP/Android-03.png)

3 . Wait for Gradle Build

![image](https://i.postimg.cc/7Y7rrDPL/Android-04.png)

4 . Install and execute the application in the connected smartphone

![image](https://i.postimg.cc/Y05RzJTn/Android-05.png)

As a result, the application is executed in the smartphone.

![image](https://i.postimg.cc/mgzHWBmh/Execute-01.png)

![image](https://i.postimg.cc/8CLfswQS/Execute-02.png)

As can be seen, the TOKEN Firebase is shown. Thus, it has to be copied by the user and pasted in the API Gateway (1) code. This process is shown in the next section.

### 2.2 Adapting API Gateway (1) to Firebase TOKEN
The API Gateway (1) performs the communication processes in _communications.js_, located in _managers_ folder, inside _APIgateway_ project.

![image](https://i.postimg.cc/R00Frnwc/adapting-01.png)

Edition takes place on _communications.js_ file so, a text editor is needed to change the file content. In this tutorial, Visual Studio Code is used but a more simple editor could be enough.

_communications.js_ counts on two points where a modification is precised. These can be identified at the code by the #TODO flag.

1 .The first one is located at _sendRequest_, around line 58. Here it is necessary to substitude YOUR_IP_HERE by the ip of the machine which executes the server.


![image](https://i.postimg.cc/pLfYbvmn/Adaptin-server-01.png)


2 .The second point to modify is precisely where the Token Firebase from the smartphone has to be pasted. This code line is located some lines below the first modification, around line 73.


![image](https://i.postimg.cc/SKmLmP91/Adaptin-server-02.png)

As a result of these two changes, execution process can be done. This demo is shown in the next section.


## 3. Demo example ##
The demo example provide a possible and potential use of the tool. This proof of concept consist of obtaining the personal information stored in the smartphone PODS (3) from the API Gateway (1). In order to perform this, it is necessary that the machine which deployes the API Gateway (1) and the smartphone are connected to the same local network. It is optional if the API Gateway (1) network allows external connections.

The first point is deploying the API Gateway (1). Thus, using command line, the prompt has to be located at the main directory of _APIgateway_. Thus, the next two commands are executed:

~~~~
$ npm install
$ node index.js
~~~~

It is recommended to run the instructions using _sudo_ permissions.

![image](https://i.postimg.cc/VNgTrDyd/16-execution.png)

As a result, the server is deployed. In order to interact with the smartphone PODS (3), the SOLID domain has to be provided. This value corresponds with the subdomain defined in the previous section. Thus, petitions can be performed using the web interface located at http://localhost:8080/docs or executing a GET petition on http://localhost:8080/card?subdomain=SUBDOMAIN (in the previous steps, the subdomain was defined as "emiliocastillo")

![image](https://i.postimg.cc/59vbRW8J/17-execution.png)

Using the interface, a GET petition to the SOLID PODS (3) can be made. Thus, specifying the SOLID domain of the user, the information is obtained.

![image](https://i.postimg.cc/fTND4qv9/18-execution.png)

Once the execution process is completed, the personal information is obtained back. Thus, the profile is represented in JSON format, providing all the details.

![image](https://i.postimg.cc/cHt0WVjQ/19-response.png)

As can be seen, the information model defined for this demo, can be found at the web interface.

![image](https://i.postimg.cc/7Ycxs1LC/20-response.png)





