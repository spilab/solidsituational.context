package org.openapitools.server.database;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public final class NotificationDAO_Impl implements NotificationDAO {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfNotificationClass;

  public NotificationDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfNotificationClass = new EntityInsertionAdapter<NotificationClass>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `notifications`(`id`,`resource`,`method`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, NotificationClass value) {
        stmt.bindLong(1, value.id);
        if (value.resource == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.resource);
        }
        if (value.method == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.method);
        }
      }
    };
  }

  @Override
  public void insertNotification(NotificationClass notification) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfNotificationClass.insert(notification);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<NotificationClass> getAll() {
    final String _sql = "SELECT * FROM notifications";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfResource = _cursor.getColumnIndexOrThrow("resource");
      final int _cursorIndexOfMethod = _cursor.getColumnIndexOrThrow("method");
      final List<NotificationClass> _result = new ArrayList<NotificationClass>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final NotificationClass _item;
        final String _tmpResource;
        _tmpResource = _cursor.getString(_cursorIndexOfResource);
        final String _tmpMethod;
        _tmpMethod = _cursor.getString(_cursorIndexOfMethod);
        _item = new NotificationClass(_tmpResource,_tmpMethod);
        _item.id = _cursor.getInt(_cursorIndexOfId);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
