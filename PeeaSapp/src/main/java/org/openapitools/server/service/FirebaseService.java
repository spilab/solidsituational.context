package org.openapitools.server.service;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.openapitools.server.MainActivity;
import org.openapitools.server.database.NotificationClass;
import org.openapitools.server.database.NotificationDatabase;
import org.openapitools.server.response.*;

import java.util.*;
import org.openapitools.server.R;


import org.openapitools.server.resource.CardResource;


public class FirebaseService extends FirebaseMessagingService {

    Gson gson = new GsonBuilder().create();

	public FirebaseService() {
    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("TOKEN FIREBASE", s);
    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...
        String TAG = "FirebaseService: ";
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Map<String, String> data = remoteMessage.getData();
            executeAPI(data);
            
       }

    }

    private void executeAPI(Map<String, String> data) {

			switch (data.get("resource")){

			case "Card":
                try {
                    CardResponse cardresponse = gson.fromJson(String.valueOf(data), CardResponse.class);
                    new CardResource(getApplicationContext()).executeMethod(cardresponse);

                    //TODO Choose what type of notification to show (toast or notification in the bar)
                    showNotification("Resource Execution: "+ cardresponse.getResource()," Method: "+ cardresponse.getMethod());
                    showToastInIntentService("Resource Execution: "+ cardresponse.getResource() + " | Method: "+ cardresponse.getMethod());

                    Intent intentCard = new Intent();
                    intentCard.setAction("NOW");

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intentCard);
                    new AsyncInsertNotification().execute(new NotificationClass(cardresponse.getResource(),cardresponse.getMethod()));

                } catch (Exception e) {
                    Log.e("Error CardResponse", e.getMessage());
                }
				break;

       
        	}
    }

    private void showToastInIntentService(final String sText) {
        final Context MyContext = this;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast toast1 = Toast.makeText(MyContext, sText, Toast.LENGTH_LONG);
                toast1.show();
            }
        });
    };

     private void showNotification(String title, String body) {

        //Intent to open APP when click in the notification.
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID="1";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel= new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription ("Android Server");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

       NotificationCompat.Builder notificationBuilder= new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true).setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL).setContentIntent(resultPendingIntent).setWhen(System.currentTimeMillis()).setSmallIcon(R.drawable.common_full_open_on_phone).setContentTitle(title).setContentText(body);
        notificationManager.notify((new Random().nextInt()),notificationBuilder.build());





    }

    class AsyncInsertNotification extends AsyncTask<NotificationClass,Void,Void> {

        @Override
        protected Void doInBackground(NotificationClass... notifications) {
            NotificationDatabase db= NotificationDatabase.getInstance(getApplicationContext());
            db.notificationDAO().insertNotification(notifications[0]);

            return null;
        }
    }


}