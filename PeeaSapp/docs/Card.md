
# Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subdomain** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**organization** | **String** |  |  [optional]
**address** | [**Address**](Address.md) |  |  [optional]
**telephone** | **Integer** |  |  [optional]
**email** | **String** |  |  [optional]



