
# Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **String** |  |  [optional]
**locality** | **String** |  |  [optional]
**region** | **String** |  |  [optional]
**postalCode** | **Integer** |  |  [optional]
**country** | **String** |  |  [optional]



