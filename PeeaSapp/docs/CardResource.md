# CardResource

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCard**](CardResource.md#getCard) | **GET** /card | Gets the user&#39;s profile


<a name="getCard"></a>
# **getCard**
> Card getCard(subdomain)

Gets the user&#39;s profile

The user&#39;s profile

### Example
```java
// Import classes:
//import org.openapitools.server.resource.CardResource;

CardResource apiInstance = new CardResource();
String subdomain = null; // String | 
try {
    Card result = apiInstance.getCard(subdomain);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CardResource#getCard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdomain** | **String**|  | [default to null]

### Return type

[**Card**](Card.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

