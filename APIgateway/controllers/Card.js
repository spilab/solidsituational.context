




'use strict';

var Card = require('../service/CardService');

module.exports.getCard = function getCard (req, res, next) {

    Card.getCard(req.swagger.params, res, next);

};
