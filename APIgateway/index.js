'use strict';

var fs = require('fs'),
http = require('http'),
path = require('path');
var dataManager = require('./managers/data')


var express = require("express");
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json({
strict: false
}));
var oasTools = require('oas-tools');
var jsyaml = require('js-yaml');
var mqtt=require('mqtt');

var topic='IntegrationSolidwithPeaas';


//TODO: Change for your configuration
var serverPort = process.env.PORT ||  8080;

//TODO: Change for your mqtt server
const mqttApp = mqtt.connect("mqtt://localhost:1883");

//TODO: Change for your mqtt server for other app to send request here
const mqttRequest = mqtt.connect("mqtt://localhost:1884");

exports.topic=topic;
exports.port=serverPort;
exports.mqttApp=mqttApp;
exports.mqttRequest=mqttRequest;
exports.app=app;

var spec = fs.readFileSync(path.join(__dirname, '/api/openapi.yaml'), 'utf8');
var oasDoc = jsyaml.safeLoad(spec);

var options_object = {
    controllers: path.join(__dirname, './controllers'),
    loglevel: 'info',
    strict: false,
    router: true,
    validator: true
};


//Listen FCM Results
app.post('/result', function (req, res) {

    //console.log("Result received from FCM");
    //console.log(req.body);
    dataManager.insertData(req.body);

    res.status(201).send({
        message: 'Received correctly!'
    });
});


oasTools.configure(options_object);

oasTools.initialize(oasDoc, app, function() {
    http.createServer(app).listen(serverPort, function() {
        console.log("App running at http://localhost:" + serverPort);
        console.log("________________________________________________________________");
        if (options_object.docs !== false) {
            console.log('API docs (Swagger UI) available on http://localhost:' + serverPort + '/docs');
            console.log("________________________________________________________________");
        }
    });
});