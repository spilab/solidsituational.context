'use strict';

var vars = require('../index');
var request = require('request');
var reply=require('./data')
var port= vars.port;

var timeOutValue = 2000;
var result;

var idRequest=0;
var reqMap = new Map();


var authorizationKey='AAAAo0RHm4o:APA91bFZnjqMIAC6AUJRSY8qjGF9E9_l0B0Ud1c6MCdp8v3PcyvaAUlDRo253v8x1xy-KG4THnmuwCGP7EVGNFQwkaL1qexpd0IJx6OeZ4o_W0ntk4yFu94AEU6tAmrDLWFXREMzPdE_'
var optionsAndroid = {
    uri: 'https://fcm.googleapis.com/fcm/send',
    method: 'POST',
    headers:{
        'Content-Type':'application/json',
        'Authorization':'key='+authorizationKey
    },
    json: true,
    body: {
        "to": "",
        "data":{
            "resource": "",
            "method": "",
            "sender":"",
            "idRequest":0,
            "params" : {

                }
            }
        }
    };

var optionsMqtt={
    "resource": "",
    "method": "",
    "sender":"",
    "idRequest": 0,
    "params": {

        }
  };

exports.sendRequest = function (body, resource, method, res){
    var id=idRequest
    idRequest++
    // __________________________________________________________________________________________________
    // 1. 
    //TODO: Put the IP of the machine which executes the server (the index.js file)
    //      this will allow receiving the request response!
    // Substitute YOUR_IP_HERE by the corresponding value... e.g. it could look like 192.168.1.130. 
    //         |
    //         |
    var sender='http://YOUR_IP_HERE:'+port+'/result'
    //         |
    //         |

    switch(body.technology){
        case "firebase":
            delete body['technology'];

            // __________________________________________________________________________________________________
            // 2. 
            //TODO: Put the Firebase Token shown in YOUR smartphone application when it's opened!
            // Substitute YOUR_APPLICATION_FIREBASE_TOKEN by the corresponding value. 
            // It looks like: fwsk7_zbW4E:APA91bFDCG7B22mteXQ0-cgSsEbgMhjp1Og_k1yXyN-6Nqw_10z916B8ZqNzsZhlcfvyT5BCKQ_UZAgYfeJV5msY5NJ72QMZ5-fZuRtYE08hcsYaL9KoigGnQ6KV4PlhzBowSHGZ9474
            //                     |
            //                     |
            optionsAndroid.body.to="YOUR_APPLICATION_FIREBASE_TOKEN"
            //                     |
            //                     |

            optionsAndroid.body.data.resource=resource
            optionsAndroid.body.data.method=method
            optionsAndroid.body.data.idRequest=id
            optionsAndroid.body.data.sender="\""+sender+"\""
            optionsAndroid.body.data.params=body

            console.log(optionsAndroid)

            reply.createRequest(id);
            reqMap.set(id, { res: res, method: method, body: body});

            request(optionsAndroid, function (error, response, body) {
                if (!error && response.statusCode == 201 || response.statusCode == 200) {
                    console.log(body)
                }else{
                    console.log("Error:" + response.body)
                    res.status(404).send('No response from firebase: '+ response.body)
                }
            });

            sendResult(id);

        break;

        default:
            res.status(405).send({
                message: 'The technology param is incorrect'
            });
        break;
    }
}

function sendResult(id){
    var obj;
    var res;
    var method;
    var params

    setTimeout(function(){
        obj = reqMap.get(id);
        res=obj.res;
        method=obj.method;
        params= obj.body;
        result=reply.getResult(id, method, params);
        reqMap.delete(id);

        console.log("Response to client");
        console.log("________________________________________________________________");
        console.log(result[0]);

        if (result.length > 0) {
            res.contentType('application/json');
            res.status(200).send(result[0]);
         } else{
             console.log("NO CONTENT found");
         }

    }, timeOutValue);
}