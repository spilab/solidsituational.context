'use strict';

var vars = require('../index');
var sender = require('../managers/communication');
var dataManager = require('../managers/data')
var app = vars.app;
var body;


/**
 * Gets the user's profile
 * The user's profile
 *
 * subdomain String 
 * returns Card
 **/
var paramsgetCard=["subdomain","technology"];
module.exports.getCard = function(req, res, next) {

    body={}

    var keys = Object.keys(req);
    /*for (var i = 0; i < keys.length; i++) {
        body[paramsgetCard[i]]=req[keys[i]].value
    }
    */
    body[paramsgetCard[0]]=req[keys[0]].value
    body[paramsgetCard[1]]="firebase"
    sender.sendRequest(body,'Card','getCard',res);

    /*
    //Parameters
    console.log(req);
    res.send({
        message: 'This is the mockup controller for getCard'
    });
    */
};

//Listen Results
app.post('/resultgetCard', function (req, res) {

    console.log("Result received")
    dataManager.insertData(req.body)

    res.status(201).send({
        message: 'Sent correctly!'
    });
});

